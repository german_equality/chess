from django.db import models
from apps.utils.piece_states import PIECE_STATES
from django.db.models.deletion import CASCADE

# Create your models here.


class ChessPiece(models.Model):
    name = models.fields.CharField(max_length=100)
    status = models.CharField(choices=PIECE_STATES)       # dentro o fuera del juego
    position = models.OneToOneField()('fisicals.Positions', on_delete=models.CASCADE)
    player = models.ForeignKey('game.ChessPlayer', related_name='player_position',on_delete=CASCADE)

    def __str__(self):
        return self.name



class Pawn(ChessPiece):
    kind_of_movement = models.CharField(max_length = 150)


class Rook(ChessPiece):
    kind_of_movement = models.CharField(max_length = 150)


class Knight(ChessPiece):
    kind_of_movement = models.CharField(max_length = 150)


class Bishop(ChessPiece):
    kind_of_movement = models.CharField(max_length = 150)
    

class Queen(ChessPiece):
    kind_of_movement = models.CharField(max_length = 150)


class King(ChessPiece):
    kind_of_movement = models.CharField(max_length = 150)
