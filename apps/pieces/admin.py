from django.contrib import admin
from .models import ChessPiece
# Register your models here.


@admin.register(ChessPiece)
class ChessPieceAdmin(admin.ModelAdmin):
    model = ChessPiece
    list_display = ['id','name']
    autocomplete_fields = ['material_id']
    
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'material':
            kwargs['queryset'] = Material.objects.order_by('name')
        return super(ChessPieceAdmin,self).formfield_for_foreignkey(db_field, request, **kwargs)