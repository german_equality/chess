from .serializers import ChessBoardSerializer, PositionsSerializer
from rest_framework import viewsets
from .models import ChessBoard, Positions


class ChessBoardViewSet(viewsets.ModelViewSet):
    queryset = ChessBoard.objects.all()
    serializer_class = ChessBoardSerializer

class PositionsViewSet(viewsets.ModelViewSet):
    queryset = Positions.objects.all()
    serializer_class = PositionsSerializer
