from .models import ChessBoard, Positions
from rest_framework import serializers


class PositionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Positions
        fields = ('__all__')


class ChessBoardSerializer(serializers.ModelSerializer):

    class Meta:
        model = ChessBoard
        fields = ('__all__')
