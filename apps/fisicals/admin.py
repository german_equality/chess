from django.contrib import admin
from .models import ChessBoard, Positions

@admin.register(Positions)
class PositionsAdmin(admin.ModelAdmin):
    model = Positions
    list_display = ['id', 'row', 'column']
    search_fields = ['name', 'enabled']


@admin.register(ChessBoard)
class ChessBoardAdmin(admin.ModelAdmin):
    model = ChessBoard
    list_display = ['id','name']
