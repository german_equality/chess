from django.apps import AppConfig


class FisicalsConfig(AppConfig):
    name = 'apps.fisicals'
