from apps.utils.chessboard_states import CHESSBOARD_STATES
from apps.utils.position_states import POSITION_STATES
from django.db import models
from django.db.models.deletion import CASCADE

# Create your models here.



class Positions(models.Model):
    chessboard = models.ForeignKey('fisicals.ChessBoard', related_name='chessboard_position',on_delete=CASCADE)
    row = models.fields.PositiveIntegerField()
    column = models.fields.CharField(max_length=1)
    state = models.CharField(choices=POSITION_STATES, max_length=100)       #occuped, under attack, litiged, free
    
    def __str__(self):
        return self.column + self.row


class ChessBoard(models.Model):

    name = models.CharField(max_length=100)
    state = models.CharField(choices=CHESSBOARD_STATES, max_length=100)   #posicion inicial o en curso o termindo
    players = models.ForeignKey('game.ChessPlayer', related_name= 'players_chessboard',on_delete=CASCADE)

    def __str__(self):
        return self.name


    
    