from .models import ChessGame, ChessPlayer
from rest_framework import serializers


class ChessGameSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ChessGame
        fields = ('__all__')



class ChessPlayerSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ChessPlayer
        fields = ('__all__')
