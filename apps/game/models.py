from django.db import models
from apps.utils.game_states import GAME_STATES
from apps.utils.chessplayer_states import CHESSPLAYER_STATES
from django.db.models.deletion import CASCADE
# Create your models here.


class ChessPlayer(models.Model):
    name = models.CharField(max_length = 150)
    turn = models.BooleanField()
    status = models.CharField(choices=CHESSPLAYER_STATES, max_length = 150)   #gano perdio empato o jugando   
    game = models.ForeignKey('game.ChessGame', related_name= 'players_game', on_delete=CASCADE)


class ChessGame(models.Model):
    date = models.DateField(auto_now=False, auto_now_add=False)
    status = models.CharField(choices=GAME_STATES, max_length = 150)      #en curso o finalizado
    result = models.CharField(max_length = 150)